# SwitchIt!

Switches words to their opposite - up/down, activate/deactivate, etc.

![Switchin' stuff](https://bytebucket.org/jbeast/switchit/raw/cb951dbf17d6460392c52bd0bced043c3d8e8ead/images/usage.gif)

## Usage

Use keyboard shortcut: `ctrl+shift+i`.

Press `F1`. Select command `Switch It!`.

## Modify

If you wish to change the words that SwitchIt! will switch on, you can modify them in `Code > Preferences > User Settings`.

## Switchables

List gratefully stolen from [atom-reverser](https://github.com/severinkaderli/atom-reverser/tree/v2.2.0)

### Array
| Keyword 1 | Keyword 2   |
|-----------|-------------|
| join      | split       |
| push      | pop         |
| reduce    | reduceRight |
| shift     | unshift     |

### Boolean
| Keyword 1 | Keyword 2 |
|-----------|-----------|
| and       | nand      |
| false     | true      |
| or        | nor       |
| xnor      | xor       |

### Brackets
| Keyword 1 | Keyword 2 |
|-----------|-----------|
| (         | )         |
| [         | ]         |
| {         | }         |
| <         | >         |

### DOM
| Keyword 1      | Keyword 2            |
|----------------|----------------------|
| getElementById | getElementsByTagName |

### Events
| Keyword 1    | Keyword 2    |
|--------------|--------------|
| audioEnd     | audioStart   |
| blur         | focus        |
| drag         | drop         |
| dragEnter    | dragLeave    |
| keyDown      | keyUp        |
| mouseDown    | mouseUp      |
| mouseEnter   | mouseLeave   |
| mouseOut     | mouseOver    |
| onMouseDown  | onMouseUp    |
| onMouseEnter | onMouseLeave |
| onMouseOut   | onMouseOver  |
| pageHide     | pageShow     |
| speechEnd    | speechStart  |
| touchEnd     | touchStart   |

### HTML
| Keyword 1 | Keyword 2 |
|-----------|-----------|
| body      | head      |
| del       | ins       |
| footer    | header    |
| noScript  | script    |
| ol        | ul        |
| tFoot     | tHead     |

### JSON
| Keyword 1  | Keyword 2      |
|------------|----------------|
| JSON.parse | JSON.stringify |
| parse      | stringify      |

### Math
| Keyword 1 | Keyword 2  |
|-----------|------------|
| atan      | tan        |
| ceil      | floor      |
| cos       | sin        |
| Math.atan | Math.tan   |
| Math.ceil | Math.floor |
| Math.cos  | Math.sin   |
| Math.exp  | Math.log   |
| Math.max  | Math.min   |
| max       | min        |

### Network
| Keyword 1 | Keyword 2  |
|-----------|------------|
| client    | server     |
| connect   | disconnect |
| offline   | online     |

### Number
| Keyword 1                | Keyword 2                |
|--------------------------|--------------------------|
| MAX_SAFE_INTEGER         | MIN_SAFE_INTEGER         |
| MAX_VALUE                | MIN_VALUE                |
| NEGATIVE_INFINITY        | POSITIVE_INFINITY        |
| Number.MAX_SAFE_INTEGER  | Number.MIN_SAFE_INTEGER  |
| Number.MAX_VALUE         | Number.MIN_VALUE         |
| Number.NEGATIVE_INFINITY | Number.POSITIVE_INFINITY |
| Number.parseInt          | Number.parseFloat        |
| parseInt                 | parseFloat               |

### Operators
| Keyword 1 | Keyword 2    |
|-----------|--------------|
| !         | ~            |
| *         | /            |
| *=        | /=           |
| &         | &#124;       |
| &&        | &#124;&#124; |
| &=        | &#124;=      |
| +         | -            |
| ++        | --           |
| +=        | -=           |
| <<        | >>           |
| <<=       | >>=          |
| <=        | >=           |
| ==        | !=           |
| ===       | !==          |

### Positions
| Keyword 1 | Keyword 2 |
|-----------|-----------|
| bottom    | top       |
| down      | up        |
| left      | right     |

### React
| Keyword 1         | Keyword 2            |
|-------------------|----------------------|
| addChangeListener | removeChangeListener |
| componentDidMount | componentWillUnmount |
| getState          | setState             |

### Single Characters
| Keyword 1 | Keyword 2 |
|-----------|-----------|
| '         | "         |
| 0         | 1         |
| x         | y         |

### Other
| Keyword 1   | Keyword 2   |
|-------------|-------------|
| []          | {}          |
| activate    | deactivate  |
| add         | remove      |
| after       | before      |
| available   | unavailable |
| background  | foreground  |
| black       | white       |
| child       | parent      |
| close       | open        |
| column      | row         |
| delete      | insert      |
| destination | source      |
| enabled     | disabled    |
| export      | import      |
| exports     | imports     |
| first       | last        |
| get         | set         |
| height      | width       |
| hidden      | visible     |
| hide        | show        |
| high        | low         |
| in          | out         |
| input       | output      |
| install     | uninstall   |
| key         | value       |
| large       | small       |
| largest     | smallest    |
| link        | unlink      |
| load        | unload      |
| long        | short       |
| major       | minor       |
| next        | previous    |
| no          | yes         |
| off         | on          |
| open        | close       |
| over        | under       |
| pause       | resume      |
| prefix      | suffix      |
| public      | private     |
| that        | this        |

