var vscode = require('vscode');
var Switcher = require('./switcher.js');

function activate(context) {

    var config = vscode.workspace.getConfiguration('switchIt');    
    var switcher = new Switcher(config.switches);
    
    var disposable = vscode.commands.registerTextEditorCommand('extension.switchIt', function (textEditor, textEditorEdit) {
      switcher.switch(textEditor);
    });

    context.subscriptions.push(config);
    context.subscriptions.push(switcher);
    context.subscriptions.push(disposable);
}
exports.activate = activate;

function deactivate() {
}

exports.deactivate = deactivate;