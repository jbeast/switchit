var vscode = require('vscode');

function Switcher(switches) {
  this._switches = switches;
  this._userHighlighted = false;
  
  this._createOppositeSwitches();
}

Switcher.prototype._createOppositeSwitches = function() {
  for (var key in this._switches) {
    if (this._switches.hasOwnProperty(key)) {
      var element = this._switches[key];
      this._switches[element] = key;
    }
  }
}

Switcher.prototype.switch = function(textEditor) {
  var self = this; 
  
  self._textEditor = textEditor;
    
  // Find the selections, has to return a Promise as we might call a command (which is asychronous)
  this._getSelections()
  
    // If we have an opposite change the word
    .then(function(selections) {
      var document = self._textEditor.document;
      var ranges = selections.map(function(selection) {
        return new vscode.Range(selection.start, selection.end);
      });
      var word = document.getText(ranges[0]);
      
      if (self.hasSwitch(word)) {
        return self._textEditor.edit(function(textEditor) {
          console.log(`Replacing ${word} with ${self.getSwitch(word)}`);
          ranges.forEach(function(range) { textEditor.replace(range, self.getSwitch(word)) });
        });
      } else {
        return Promise.resolve();
      }
    })
    
    // Deselect the selection if we had to make it ourselves
    .then(function() {
      if (!self._userHighlighted) { 
        return vscode.commands.executeCommand('cancelSelection');
      }
    })
    ;
};

Switcher.prototype.hasSwitch = function(word) {
  return this._switches.hasOwnProperty(word);
}

Switcher.prototype.getSwitch = function(word) {
  if (!this.hasSwitch(word)) return word;
  return this._switches[word];
}

Switcher.prototype._getSelections = function() {
  var self = this;
  return new Promise(function(resolve, reject) {
    
    var selections = self._textEditor.selections;

    if (selectionsIsEmpty(selections)) {
      vscode.commands.executeCommand('editor.action.addSelectionToNextFindMatch')
        .then(
          function() {
            self._userHighlighted = false;
            resolve(self._textEditor.selections);
          },
          function() {
            reject('Command editor.action.addSelectionToNextFindMatch failed');
          }
        );
    } else {
      self._userHighlighted = true;
      resolve(selections)
    }
  });
};

function selectionsIsEmpty(selections) {
  if (selections.length > 1) return false;
  return (selections[0].start.character == selections[0].end.character) 
    && (selections[0].start.line == selections[0].end.line);
}

module.exports = Switcher;