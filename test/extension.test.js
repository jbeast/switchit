/* global suite, test, setup */

var assert = require('assert');
var Switcher = require('../switcher.js');

suite("Switcher", function() {
    var switcher;
  
    setup(function() {
      switcher = new Switcher({up: "down", left: "right", sleep: "wake up"});
    });

    suite("new", function() {
      test("adds the opposites to the internal switches", function() {
        assert.equal(switcher.getSwitch("down"), "up");
        assert.equal(switcher.getSwitch("right"), "left");
      });
    });
    
    suite("hasSwitch", function() {
      test("returns true when switch exists", function() {
        assert(switcher.hasSwitch("down"));
        assert(switcher.hasSwitch("right"));
      })
      
      test("returns false when switch doesn't exist", function() {
        assert(!switcher.hasSwitch("monkey"));
        assert(!switcher.hasSwitch("news"));
      })
    })
    
    suite("getSwitch", function() {
      test("returns a switch when it exists", function() {
        assert.equal(switcher.getSwitch("up"), "down");
      })
      
      test("it works with multple words", function() {
        assert.equal(switcher.getSwitch("sleep"), "wake up");
      })
      
      test("returns the word given when a switch doesn't exist", function() {
        assert.equal(switcher.getSwitch("monkey"), "monkey")
      })
    })
    
});